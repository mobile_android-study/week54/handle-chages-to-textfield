import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Retrieve text input',
      home: MyCustomForm(),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

class _MyCustomFormState extends State<MyCustomForm> {
  final myController = TextEditingController();

  @override
  void initState() {
    super.initState();
    myController.addListener(_printLastestValue);
  }
  void dispose() {
    myController.dispose();
    super.dispose();
  }
  void _printLastestValue() {
    print('Second text field : ${myController.text}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Retrieve text input'),
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextField(
                onChanged: (text) {
                  print('First text field: $text');
                },
              ),
              TextField(  
                controller: myController,
              )
            ],
          )),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              showDialog(
                context: context, 
                builder: (context) {
                  return AlertDialog(
                    content: Text(myController.text),
                  );
                });
          },
          tooltip: 'Show me the value',
          child: Icon(Icons.text_fields),
          ),
    );
  }
}
